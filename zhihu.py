import os
import requests
from selenium_base import *
from slide_crack import SlideCrack

url = 'https://www.zhihu.com/signin?next=%2F'
username = '' # 知乎账号
password = '' # 知乎密码


def download_img(url):
    headers = {
        'authority': 'necaptcha.nosdn.127.net',
        'pragma': 'no-cache',
        'cache-control': 'no-cache',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_16_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.92 Safari/537.36',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'sec-fetch-site': 'none',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-user': '?1',
        'sec-fetch-dest': 'document',
        'accept-language': 'zh-CN,zh;q=0.9,en;q=0.8',
    }

    filename = url.split('/')[-1]
    req = requests.get(url, headers=headers)
    path = os.path.join('imgs', filename)
    with open(path, 'wb') as f:
        f.write(req.content)
    return path


try:
    brower = get_brower(headless=False)
    brower.get(url)
    tab = brower.find_element_by_css_selector(
        '#root > div > main > div > div > div > div.SignContainer-content > div > form > div.SignFlow-tabs > div:nth-child(2)')
    tab.click()
    username_element = brower.find_element_by_css_selector(
        '#root > div > main > div > div > div > div.SignContainer-content > div > form > div.SignFlow-account > div > label > input')
    username_element.send_keys(username)
    password_element = brower.find_element_by_css_selector(
        '#root > div > main > div > div > div > div.SignContainer-content > div > form > div.SignFlow-password > div > label > input')
    password_element.send_keys(password)
    submit_css = '#root > div > main > div > div > div > div.SignContainer-content > div > form > button'
    wait_element_css(brower, submit_css)
    submit = brower.find_element_by_css_selector(submit_css)
    time.sleep(1)
    submit.click()

    bg_url = None
    for i in range(5):
        bg_css = 'body > div.yidun_popup--light.yidun_popup > div.yidun_modal__wrap > div > div > div.yidun_modal__body > div > div.yidun_panel > div > div.yidun_bgimg > img.yidun_bg-img'
        try:
            wait_element_css(brower, bg_css, 5)
        except:
            submit.click()
        bg_url = brower.find_element_by_css_selector(bg_css).get_attribute('src')
        if bg_url:
            break
        else:
            time.sleep(0.5)
    bg_path = download_img(bg_url)

    target_css = 'body > div.yidun_popup--light.yidun_popup > div.yidun_modal__wrap > div > div > div.yidun_modal__body > div > div.yidun_panel > div > div.yidun_bgimg > img.yidun_jigsaw'
    wait_element_css(brower, target_css)
    target_url = brower.find_element_by_css_selector(target_css).get_attribute('src')
    target_path = download_img(target_url)
    out_path = 'imgs/out.png'
    sc = SlideCrack(target_path, bg_path, out_path)
    offset_x = sc.discern()

    slider = brower.find_element_by_css_selector('body > div.yidun_popup--light.yidun_popup > div.yidun_modal__wrap > div > div > div.yidun_modal__body > div > div.yidun_control > div.yidun_slider')
    action = webdriver.ActionChains(brower)
    offset_x = offset_x / 1.5 + 10
    action.click_and_hold(slider).move_by_offset(offset_x, 0)
    time.sleep(0.5)
    action.release().perform()
    print('success')
except:
    print('error')
finally:
    time.sleep(5)
    cookies = brower.get_cookies()
    get_brower_header(brower)
    print(cookies)
    brower.close()
